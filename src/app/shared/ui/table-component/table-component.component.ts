import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { FormFieldType, FormField } from "./../constants/formFields";

interface IColumn {
  key: string;
  displayTitle: string;
}

@Component({
  selector: 'app-table-component',
  templateUrl: './table-component.component.html',
  styleUrls: ['./table-component.component.scss']
})
export class TableComponentComponent implements OnInit {

  @Input('tableData')
  tableData: FormField[] = [];

  @Output('tableRowClick')
  tableRowClick = new EventEmitter();

  columns: IColumn[] = [];
  displayColumns: string[] = [];
  selectedElement = null;

  tableDataSource: MatTableDataSource<any> = new MatTableDataSource(this.tableData);

  constructor() { }

  ngOnInit(): void {
    this.tableDataSource = new MatTableDataSource(this.tableData);
    const testRecord: any = this.tableData[0];
    for(let key in testRecord) {
      let column: IColumn = {key: key, displayTitle: testRecord[key]['displayTitle']};
      this.displayColumns.push(key);
      this.columns.push(column);
    }
    this.onTableRowClick(null, testRecord);
  }

  onTableRowClick(event: Event | null, row: any) {
    this.selectedElement = row;
    this.tableRowClick.emit(row);
  }

}
