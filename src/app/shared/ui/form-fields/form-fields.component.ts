import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { FormField, FieldType } from './../constants/formFields';

@Component({
  selector: 'app-form-fields',
  templateUrl: './form-fields.component.html',
  styleUrls: ['./form-fields.component.scss'],
})
export class FormFieldsComponent implements OnInit {
  @Input('fieldValue') fieldValue: any;
  @Input('fieldChange') fieldChange = new EventEmitter();
  fieldType = FieldType;
  inputTimeout: number  | undefined = 0;

  constructor() {}

  ngOnInit(): void {}

  onInputChange($event: any, fieldVal: any) {
    clearTimeout(this.inputTimeout);
    this.inputTimeout = setTimeout(() => {
      const fieldValue = $event.target && $event.target.value;
      if (fieldVal.validateFn) {
        const errorState = fieldVal.validateFn(
          $event.target && $event.target.value
        );
        fieldVal.hasError = errorState.hasError;
        fieldVal.errMsg = errorState.errorMessage;
        if (errorState.hasError) {
          return;
        }
      }

      if (fieldValue !== this.fieldValue.value) {
        this.fieldValue.value = fieldValue;
        this.fieldChange.emit(this.fieldValue);
      }
    }, 300);
  }

  onLinkClick($event: Event, fieldVal: any) {}
}
