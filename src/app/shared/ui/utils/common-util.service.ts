import { Injectable } from '@angular/core';
import { FormFieldType, FormField } from "./../constants/formFields";

@Injectable({
  providedIn: 'root'
})
export class CommonUtilService {

  constructor() { }

  sanitiesFields(keyMapping: any, data: any, formType = FormFieldType.FORM, validationFns: any = {}) {
    const sanitizedFinalResult: {}[] = [];
    data.forEach((details: any) => {
      const mappedDetail: any = {};
      for(let key in details ) {
        const field: FormField = {
          key: key,
          displayTitle: keyMapping[key]['displayTitle'],
          type: keyMapping[key]['type'],
          permission: keyMapping[key]['permission'],
          value: details[key],
          formFieldType: formType,
          hasError: false,
          validateFn: validationFns[key],
        } 
        mappedDetail[key] = field;
      }
      sanitizedFinalResult.push(mappedDetail);
    });

    return sanitizedFinalResult;
  }

}
