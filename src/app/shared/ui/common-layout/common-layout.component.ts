import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-common-layout',
  templateUrl: './common-layout.component.html',
  styleUrls: ['./common-layout.component.scss'],
})
export class CommonLayoutComponent implements OnInit {
  @Input('commonParams') commonParams: any;

  fieldValues: any;
  isFormEdit = false;
  isTableEdit = false;

  constructor() {}

  ngOnInit(): void {}

  onTableRowClick($event: any) {
    if (!this.isTableEdit) {
      this.resetForm();
      this.fieldValues = {};
      const selectedField = JSON.parse(JSON.stringify($event));
      for (let key in selectedField) {
        selectedField[key]['validateFn'] = $event[key]['validateFn'];
        this.fieldValues[key] = selectedField[key];
        this.fieldValues[key]['formFieldType'] = 'form';
      }
    }
  }

  onTableEditClick($event: Event) {
    this.isTableEdit = true;
    this.commonParams.tableData.forEach((element: any) => {
      for (let key in element) {
        if (key === 'nme' || key === 'zip') element[key]['type'] = 'input';
      }
    });
  }

  onTableCancelClick() {
    this.onResetTableForm();
  }

  onTableSaveClick() {
    this.onResetTableForm();
  }

  onResetTableForm() {
    this.isTableEdit = false;
    this.commonParams.tableData.forEach((element: any) => {
      for (let key in element) {
        if (key === 'nme' || key === 'zip') element[key]['type'] = 'label';
      }
    });
  }

  onEditClick($event: Event) {
    this.isFormEdit = true;
    for (let key in this.fieldValues) {
      this.fieldValues[key]['type'] = 'input';
    }
  }

  onFormCancelClick() {
    this.resetForm();
  }

  onFormSaveClick() {
    this.resetForm();
  }

  resetForm() {
    this.isFormEdit = false;
    for (let key in this.fieldValues) {
      this.fieldValues[key]['type'] = 'label';
    }
  }
}
