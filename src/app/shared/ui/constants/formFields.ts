export interface FormField  {
    key: string;
    displayTitle: string;
    type: string;
    permission: string;
    value: string | number | boolean | null | undefined;
    formFieldType: string;
    hasError: false;
    validateFn?: any;
    fieldAutoCorrectFn?: any;
    placeHolder?: string;
    multipleSelect?: boolean;
    mandate?: boolean;
    disabled?: boolean;
    dropDownValue?: any;
    dateFilter?: any;
    errMsg?: string;
}

export const FieldType = {
    LABEL: 'label',
    INPUT: 'input',
    LINK: 'link'
}

export const FormFieldType = {
    TABLE: 'table',
    FORM: 'form',
}