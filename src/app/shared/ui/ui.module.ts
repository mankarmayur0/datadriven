import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialExampleModule } from './../../material.module';
import { CommonLayoutComponent } from './common-layout/common-layout.component';
import { FormFieldContainerComponent } from './form-field-container/form-field-container.component';
import { FormFieldsComponent } from './form-fields/form-fields.component';
import { TableComponentComponent } from './table-component/table-component.component';

const UIElement = [FormFieldContainerComponent, FormFieldsComponent, TableComponentComponent];
const CommonElements = [CommonLayoutComponent];

@NgModule({
  declarations: [...UIElement, ...CommonElements],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MaterialExampleModule
  ],
  exports: [...UIElement, ...CommonElements],
})
export class UIModule { }
