import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-form-field-container',
  templateUrl: './form-field-container.component.html',
  styleUrls: ['./form-field-container.component.scss']
})
export class FormFieldContainerComponent implements OnInit {
  @Input('fieldValues') fieldValues = null;


  constructor() { }

  ngOnInit(): void {
  }

}
