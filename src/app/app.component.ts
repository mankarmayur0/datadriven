import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Data Driven App';

  routes =  [
    { path: '', label: 'Home' },
    { path: '/customers',  label: 'Customers' },
    { path: '/orders', label: 'Orders'}
   ];
}
