import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRoutingModule } from './orders-routing.module';
import { OrdersComponent } from './orders.component';

import { UIModule } from '../shared/ui/ui.module';

@NgModule({
  imports: [
    CommonModule,
    OrdersRoutingModule,
    UIModule
  ],
  declarations: [OrdersComponent]
})
export class OrdersModule { }
