export const keyMapping = {
    nme: {
        displayTitle: "Name",
        type: 'label',
        permission: 'read-only'
    },
    cno: {
        displayTitle: "Cust no",
        type: 'label',
        permission: 'read-only'
    },
    cty: {
        displayTitle: "City",
        type: 'label',
        permission: 'read-only'
    },
    ste: {
        displayTitle: "State",
        type: 'label',
        permission: 'read-only'
    },
    zip: {
        displayTitle: "Zip code",
        type: 'label',
        permission: 'read-only'
    },
    phno: {
        displayTitle: "Phone number",
        type: 'label',
        permission: 'read-only'
    },
    sales: {
        displayTitle: "Sales",
        type: 'label',
        permission: 'read-only'
    },
}

export const customerData = [
    { cno: "0030", nme: "CANNON TOOLS CO", cty: "ATLANTA", ste: "GA", zip: 303012334, sales: 23442.00, phno: '9878765987' },
    { cno: "0021", nme: "INTERNATIONAL BANK CORP", cty: "NEW YORK", ste: "NY", zip: 100059989, sales: .00, phno: '9878765987' },
    { cno: "0130", nme: "SUN DIAL CITRUS GROWERS", cty: "LOS ANGELES", ste: "CA", zip: 902130000, sales: 21489.00, phno: '9878765987' },
    { cno: "0150", nme: "IMPERIAL BANKCORP", cty: "NEW YORK", ste: "NY", zip: 100190000, sales: .00, phno: '9878765987' },
    { cno: "0170", nme: "UNITED ATLANTIC SHARES", cty: "CHARLOTTE", ste: "NC", zip: 282552550, sales: .00, phno: '9878765987' },
    { cno: "0120", nme: "SOUTHWEST STATE OIL REFINING", cty: "SHERMAN OAKS", ste: "CA", zip: 914231423, sales: .00, phno: '9878765987' },
    { cno: "0210", nme: "WEST LIFE INSURANCE", cty: "BALTIMORE", ste: "MD", zip: 212031203, sales: .00, phno: '9878765987' },
    { cno: "0230", nme: "CHEMICAL MUTUAL", cty: "FORT WORTH", ste: "TX", zip: 761026102, sales: 931.72, phno: '9878765987' },
    { cno: "0250", nme: "MICHIGAN LIGHTING INC.", cty: "PHOENIX", ste: "AZ", zip: 850365036, sales: .00, phno: '9878765987' },
]