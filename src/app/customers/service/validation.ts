export const validations = {
  nme: (value: any) => {
    if (value === null || value === undefined || value === '') {
      return { errorMessage: 'Field can not be empty', hasError: true };
    } else {
      return { errorMessage: '', hasError: false };
    }
  },
  zip: (value: any) => {
    if ((value * 0).toString() === 'NaN') {
      return { errorMessage: 'Zip code should be number.', hasError: true };
    } else {
      return { errorMessage: '', hasError: false };
    }
  },
};
