import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomersRoutingModule } from './customers-routing.module';
import { CustomersComponent } from './customers.component';
import { UIModule } from '../shared/ui/ui.module';

@NgModule({
  imports: [
    CommonModule,
    CustomersRoutingModule,
    UIModule
  ],
  declarations: [CustomersComponent]
})
export class CustomersModule { }
