import { Component, OnInit } from '@angular/core';
import { CommonUtilService } from './../shared/ui/utils/common-util.service';
import {keyMapping,customerData} from './constant/customer.constant'
import { validations } from './service/validation';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  commonParams: any = {};

  constructor(private commonUtilService: CommonUtilService) { }

  ngOnInit() {
    const sanitizedDetails = this.commonUtilService.sanitiesFields(keyMapping, customerData, 'table', validations);
    this.commonParams['tableData'] = sanitizedDetails;

  }

}
